from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
import matplotlib.pyplot as plt
import numpy as np

fig = plt.figure()
ax = fig.gca(projection='3d')
X = np.arange(-5, 5, 0.25)
Y = np.arange(-5, 5, 0.25)
X, Y = np.meshgrid(X, Y)
R = np.exp(-(((X+1)/2.)**2 + (Y-0.5)**2))

surf = ax.plot_surface(X, Y, R, rstride=1, cstride=1, cmap=cm.coolwarm,
        linewidth=0, antialiased=False)
ax.set_zlim(0, 1.01)

ax.zaxis.set_major_locator(LinearLocator(10))
ax.zaxis.set_major_formatter(FormatStrFormatter('%.02f'))

fig.colorbar(surf, shrink=0.5, aspect=5)
ax.set_axis_off()
plt.show()


cos_window = np.outer(np.hanning(im2.shape[0]), np.hanning(im2.shape[1]))

im3 = np.multiply(im2, cos_window[:,:,None])

im4 = np.roll(im2, 50, axis=0)
im4 = np.multiply(im4, cos_window[:,:,None])
plt.imshow(im4/255.)

im4 = np.roll(im2, -50, axis=0)
im4 = np.multiply(im4, cos_window[:,:,None])
plt.imshow(im4/255.)

im4 = np.roll(im2, 50, axis=1)
im4 = np.multiply(im4, cos_window[:,:,None])
plt.imshow(im4/255.)

im4 = np.roll(im2, -50, axis=1)
im4 = np.multiply(im4, cos_window[:,:,None])
plt.imshow(im4/255.)